import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

function textAnimate(index) {
  const text = gsap.timeline({
    scrollTrigger: {
      trigger: ".section-about",
      start: "top 0%",
      end: "bottom",
      scrub: 0.5,
    },
  });
  text.fromTo(
    `.article-${index}`,
    {
      opacity: 1,
      x: 0,
    },
    {
      opacity: 0,
      x: index == 1 ? -200 : 200,
    }
  );
}

(function main() {
  textAnimate(1);
  textAnimate(2);
})();
