import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

const textsList = document.querySelectorAll(".section-text");

function textAnimate(className, index) {
  const text = gsap.timeline({
    scrollTrigger: {
      trigger: `.${className}`,
      start: "top",
      end: "bottom",
      pin: true,
      scrub: 0.5,
      onUpdate(self) {
        if (self.direction === -1 && self.progress > 0.9) {
          self.scroll(self.start);
        }
        console.log(self.progress);
      },
    },
  });
  text.fromTo(
    `.${className}`,
    {
      opacity: 0,
      y: 400,
    },
    {
      duration: 1,
      opacity: 1,
      y: 0,
    }
  );
}

(function main() {
  textsList.forEach((elementRef, index) => {
    const className = `text-${index + 1}`;
    elementRef.classList.add(className);
    textAnimate(className, index);
  });
})();
